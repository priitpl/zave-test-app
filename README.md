# Technical info
* MySQL as the main storage server.
* [Redis.io] as for cache server. Used it over memcache because of it simplicity, possibilities, speed and scalability.
* PHP framework: Symfony2. You can read about benefits here, but has big community, a lot of available bundles, easy to use, good Dependency injector and lot more.
* Twig as template engine - it's build into Symfony(but you can change to what ever engine). It's very easy to learn, if you know Smarty or Django template engine.
* [Twitter Bootstrap] CSS framework
* [bower] <http://bower.io/> as package manager for frontend
* [AngularJS] - frontend application: form submission/validation, messages fetching and rendering
* [Grunt] <http://gruntjs.com/> is for packing all javascript codes toger into one package and uglifying it.
<?php

namespace Priit\ZaveGuestBookBundle\Controller;

use Priit\ZaveGuestBookBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Entry point.
     *
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $guestbookService = $this->get('priit_zave_guest_book.guestbook_service');


        $responseData = [
            'item' => new Message(),
            'messages' => []
        ];

        // On post, process form
        if ($request->getMethod() == 'POST'){

            $data = $request->request;

            // create message and redirect, if data is valid.
            if ($guestbookService->isValid($data)) {
                $guestbookService->createMessage($data);

                return $this->redirect($this->generateUrl('index'));

            }
            // Otherwise display errors
            else {
                $responseData['error_fields'] = $guestbookService->getFieldsErrors();
                $responseData['item'] = $data->all();

            }

        }

        $responseData['messages'] = $guestbookService->getList();

        return $responseData;
    }


    /**
     * @Route("/about", name="about")
     * @Template()
     * @return array
     */
    public function aboutAction()
    {
        return [];
    }
}

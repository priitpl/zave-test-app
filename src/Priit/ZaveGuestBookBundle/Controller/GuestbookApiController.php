<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:33
 */

namespace Priit\ZaveGuestBookBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GuestbookApiController.
 * REST Service.
 *
 *
 * @package Priit\ZaveGuestBookBundle\Controller
 * @Route("/api")
 */
class GuestbookApiController extends Controller
{
    /**
     * @param $data
     * @return JsonResponse
     */
    protected function createResponse($success, $data)
    {
        return new JsonResponse($data);
    }

    public function listAction(Request $request)
    {
        $data = [];
        return $this->createResponse(true, $data);
    }

    public function getAction(Request $request)
    {
        return $this->createResponse(true, []);
    }


    public function createAction(Request $request)
    {
        $service = $this->get('priit_zave_guest_book.guestbook_service');

        $newMessage = $service->createMessage($request->request);

        return $this->createResponse(true, []);
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:24
 */

namespace Priit\ZaveGuestBookBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait EntityIdentityTrait
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @return integer|null
     */
    public function getId()
    {
        return $this->id;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:24
 */

namespace Priit\ZaveGuestBookBundle\Traits;


/**
 * Class ClazzTrait
 * @package Priit\ZaveGuestBookBundle\Traits
 */
trait ClazzTrait
{

    static public function clazz()
    {
        return get_called_class();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:24
 */

namespace Priit\ZaveGuestBookBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EntityLoggableTrait
 * @package Priit\ZaveGuestBookBundle\Traits
 *
 */
trait EntityLoggableTrait
{
    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get created at datetime.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * Set created at datetime.
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * Set created at datetime on persist.
     *
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }
}
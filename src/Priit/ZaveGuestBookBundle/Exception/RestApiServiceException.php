<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:37
 */

namespace Priit\ZaveGuestBookBundle\Exception;


class RestApiServiceException extends AbstractServiceException
{
    static public function fieldIsNotValid($field, $fieldError)
    {
        return new self($fieldError, 100);
    }

}
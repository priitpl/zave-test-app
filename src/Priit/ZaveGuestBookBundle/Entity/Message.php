<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:51
 */

namespace Priit\ZaveGuestBookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Priit\ZaveGuestBookBundle\Traits\EntityIdentityTrait;
use Priit\ZaveGuestBookBundle\Traits\EntityLoggableTrait;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * Class Message
 * @package Priit\ZaveGuestBookBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="guestbook_messages")
 */
class Message extends AbstractBaseEntity
{
    /**
     * @ORM\Column(name="name", length=255, nullable=false)
     * @var
     */
    private $name;


    /**
     * @ORM\Column(name="email", type="string", nullable=false)
     * @var
     */
    private $email;


    /**
     * @ORM\Column(type="text", nullable=false)
     * @var
     */
    private $content;


    /**
     * Serialize object.
     *
     * @return string
     */
    public function seriazize()
    {
        return json_encode([
            'id' => $this->getId(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'content' => base64_encode($this->getContent()),
            'created_at' => $this->getCreatedAt()->format(DATE_ISO8601)
        ]);
    }


    /**
     * Unserialize - inject right into current object
     *
     * @param $string
     * @return $this
     */
    public function unserialize($string)
    {
        $data = json_decode($string, true);

        $this->id = $data['id'];
        $this->setName($data['name']);
        $this->setEmail($data['email']);
        $this->setContent(base64_decode($data['content']));
        $this->setCreatedAt(new \DateTime($data['created_at']));

        return $this;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Message
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Message
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}

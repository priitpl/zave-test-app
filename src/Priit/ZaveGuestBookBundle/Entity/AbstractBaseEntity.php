<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:51
 */

namespace Priit\ZaveGuestBookBundle\Entity;


use Priit\ZaveGuestBookBundle\Traits\ClazzTrait;
use Doctrine\ORM\Mapping as ORM;
use Priit\ZaveGuestBookBundle\Traits\EntityIdentityTrait;
use Priit\ZaveGuestBookBundle\Traits\EntityLoggableTrait;

/**
 * Class AbstractBaseEntity
 * @package Priit\ZaveGuestBookBundle\Entity
 *
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks()
 */
abstract class AbstractBaseEntity
{
    use EntityIdentityTrait;

    use EntityLoggableTrait;

    use ClazzTrait;
}
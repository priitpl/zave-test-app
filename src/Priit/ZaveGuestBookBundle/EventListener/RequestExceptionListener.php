<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:47
 */

namespace Priit\ZaveGuestBookBundle\EventListener;

use Priit\ZaveGuestBookBundle\Exception\RestApiServiceException;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RequestExceptionListener
{

    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $route)
    {
        $this->router = $route;
    }


    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof RestApiServiceException){

            $response = new JsonResponse(array(
                'success' => false,
                'message'=>$exception->getMessage(),
                'data' => $exception->getData(),
                'code' => $exception->getServiceCode()
            ));
            $response->setStatusCode(403);

            $event->setResponse($response);

        } else if ($exception instanceof NotFoundHttpException){
            $response = new RedirectResponse($this->router->generate('index'));
            $event->setResponse($response);

        }
    }
}
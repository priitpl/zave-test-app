<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 22.10.15
 * Time: 9:38
 */

namespace Priit\ZaveGuestBookBundle\Service;


use Doctrine\ORM\EntityManager;
use Priit\ZaveGuestBookBundle\Entity\Message;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EmailValidator;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GuestbookService
{

    const REDIS_MESSAGE_PATTER = 'message:%s';

    /**
     * @var EntityManager
     */
    private $em;


    /**
     * @var
     */
    private $redis;


    /**
     * @var
     */
    private $fieldsErrors = [];

    /**
     * @var RecursiveValidator
     */
    private $validator;


    /**
     * @param EntityManager $em
     * @param $redisClient
     * @param RecursiveValidator $validator
     */
    public function __construct(EntityManager $em, $redisClient, ValidatorInterface $validator)
    {
        $this->em = $em;

        $this->redis = $redisClient;

        $this->validator = $validator;
    }


    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository()
    {
        return $this->em->getRepository(Message::clazz());
    }


    /**
     * Get required fields.
     * @return array
     */
    protected function getRequiredFields()
    {
        return ['name', 'email', 'content'];
    }


    /**
     * Add field error.
     *
     * @param $field
     * @param $error
     * @return $this
     */
    protected function addFieldError($field, $error)
    {
        if (is_array($error)){
            foreach($error as $_e){
                $this->addFieldError($field, $_e);
            }

        } else {
            $this->fieldsErrors[$field][] = $error;

        }

        return $this;
    }


    /**
     * Check, if field error exists.
     *
     * @param $field
     * @return bool
     */
    protected function fieldErrorExceptionExists($field)
    {
        return array_key_exists($field, $this->fieldsErrors);
    }


    /**
     * Get fields errors
     * @return mixed
     */
    public function getFieldsErrors()
    {
        return $this->fieldsErrors;
    }


    /**
     * Validate data.
     *
     * @param ParameterBag $data
     * @return bool
     */
    public function isValid(ParameterBag $data)
    {
        $this->fieldsErrors = [];

        foreach($this->getRequiredFields() as $field)
        {
            if (!$value = $data->get($field)) {
                $this->addFieldError($field, 'Required field is empty or wrong');
            }
        }

        $email = $data->get('email', null);

        $emailErrors = $this->validator->validate($email, new Email());

        if (!$this->fieldErrorExceptionExists('email') && $emailErrors->count())
        {
            $this->addFieldError('email', 'Invalid email');
        }

        return !(count($this->fieldsErrors) > 0);
    }


    /**
     * Create new message.
     * This validates the data and creates new message if everything is ok.
     *
     * @param ParameterBag $data
     * @return bool|Message
     */
    public function createMessage(ParameterBag $data)
    {
        if (!$this->isValid($data)) {
            return false;
        }

        $message = new Message();

        $message
            ->setName($data->get('name'))
            ->setEmail($data->get('email'))
            ->setContent($data->get('content'))
        ;

        $this->em->persist($message);
        $this->em->flush();
        var_dump($message->getId());

        return $message;
    }


    /**
     * @todo implement
     * @param ParameterBag $data
     */
    public function updateMessage(ParameterBag $data)
    {

    }


    /**
     * @param $id
     * @return bool
     */
    public function deleteMessage($id)
    {
        $message = $this->getRepository()->find($id);

        if (!$message) {
            return;
        }

        // Remove from cache.
        $this->redis->del(sprintf(self::REDIS_MESSAGE_PATTER, $id));

        // Remove from database
        $this->em->remove($message);
        $this->em->flush();
    }


    /**
     * Retrieve message by id.
     *
     * @param $id
     * @return null|object|void
     */
    public function getMessage($id)
    {

        $redisMessageToken = sprintf(self::REDIS_MESSAGE_PATTER, $id);

        // Search in cache.
        $redisData = $this->redis->get($redisMessageToken);

        if ($redisData) {
            $message = new Message();

            try {
                $message->unserialize($redisData);
                return $message;
            }catch (\Exception $e){

            }
        }

        /** @var  $message Message */
        $message = $this->em->getRepository(Message::clazz())->find($id);

        if (!$message) {
            return;
        }

        // Store in cache
        $this->redis->set($redisMessageToken, $message->seriazize());

        return $message;
    }


    /**
     * @return array
     */
    public function getList()
    {
        $messages = $this
            ->getRepository()
            ->findBy([], ['createdAt'=> 'DESC'])
        ;

        return $messages;
    }
}